# Boredom Strikes Again - in React

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)
[![React Framework](https://img.shields.io/badge/-React-blue?logo=react)](https://reactjs.org)
[![TailwindCSS](https://img.shields.io/badge/-TailwindCSS-black?logo=tailwindcss)](https://tailwindcss.com)

A simple project focussing on using Components for all parts of the user interface.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Acknowledgements](#acknowledgements)
- [Maintainers](#maintainers)
- [License](#license)

## Background

The project is a sample code repository for a "mini" task as part of the Front-End module for an Accelerate course.


The goals for this repository are:

1. Create a simple application using React
2. Make use of a Component where ever possible
3. Separate concerns where logical
4. Use a reducer to manage a complex state object

## Install

This project uses [node](http://nodejs.org) and [npm](https://npmjs.com). Go check them out if you don't have them locally installed.

```sh
npm install 
# Install the dependencies for the project.
```

## Usage

This is only a documentation package. Here, you can read about the project [SPEC.md](./SPEC.md)

```sh
$ npm start
# Run a local React server
```

## Acknowledgements

- [BoredAPI](https://gitlab.com/sumodevelopment) - Find something to do!
- [TailwindcSS](https://tailwindcss.com) - Utility First CSS Library

## Maintainers

[@sumodevelopment](https://gitlab.com/sumodevelopment).

## Contributing

Feel free to dive in! [Open an issue](https://gitlab.com/sumodevelopment/boredom-strikes-again-in-react/-/issues) or submit PRs.

## License

[MIT](./LICENSE.md) © Dewald Els
