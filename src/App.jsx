import { BrowserRouter, Route, Routes } from "react-router-dom";
import Bored from "./features/Bored/Bored";

function App() {
	return (
		<BrowserRouter>
			<div className="App">
				<Routes>
					<Route path="/" element={<Bored />} />
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
