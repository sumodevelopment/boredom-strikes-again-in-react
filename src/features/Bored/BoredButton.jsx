function BoredButton({ onClick, children }) {
	return (
		<button
			onClick={onClick}
			className="bg-blue-500 text-white p-4 rounded font-semibold border-2 border-slate-900">
			{children}
		</button>
	);
}
export default BoredButton;
