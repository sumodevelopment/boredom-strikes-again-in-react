function BoredHistory({ history }) {
	return (
		<section className="mt-4 p-4 border-2 border-slate-300 rounded-lg">
			<h4 className="text-lg mb-4 text-slate-700">History</h4>
			{history.length === 0 && <p>No history</p>}
			<ul className="text-slate-400">
				{history.map((activity) => (
					<li className="border-b border-slate-300 p-2" key={activity.key}>
						{activity.activity}
					</li>
				))}
			</ul>
		</section>
	);
}
export default BoredHistory;
