function BoredHeader() {
	return (
		<header className="mb-4 text-center">
			<figure className="mb-4">
				<img src="bored.png" width="500" alt="A bored pug" className="block" />
			</figure>
			<h1 className="text-4xl mb-4">Boredom Strikes Again</h1>
			<p className="text-slate-900">
				Feeling bored? Click the button to find something to do.
			</p>
		</header>
	);
}
export default BoredHeader;
