export const ACTION_BORED_FETCH_ACTIVITY = "[bored] FETCH";
export const ACTION_BORED_SET_ACTIVITY = "[bored] SET_ACTIVITY";
export const ACTION_BORED_SET_ERROR = "[bored] SET_ERROR";
export const ACTION_BORED_SET_PARTICIPANTS = "[bored] SET_PARTICIPANTS";
export const ACTION_BORED_SET_ACTIVITY_TYPE = "[bored] SET_ACTIVITY_TYPE";

export const INITIAL_STATE = {
	loading: false,
	activity: null,
	error: "",
	activityType: "",
	participants: 0,
	history: [],
};

export const boredFetchAction = () => ({
	type: ACTION_BORED_FETCH_ACTIVITY,
});

export const boredSetActivityAction = (activity) => ({
	type: ACTION_BORED_SET_ACTIVITY,
	payload: activity,
});

export const boredSetErrorAction = (error) => ({
	type: ACTION_BORED_SET_ERROR,
	payload: error,
});

export const boredSetParticipants = (participants) => ({
	type: ACTION_BORED_SET_PARTICIPANTS,
	payload: participants,
});

export const boredSetActivityType = (type) => ({
	type: ACTION_BORED_SET_ACTIVITY_TYPE,
	payload: type,
});

export const boredReducer = (state, action) => {
	switch (action.type) {
		case ACTION_BORED_FETCH_ACTIVITY:
			return {
				...state,
				error: "",
				loading: true,
			};

		case ACTION_BORED_SET_ACTIVITY:
			return {
				...state,
				loading: false,
				activity: action.payload,
				history: [action.payload, ...state.history.filter(h => h.key !== action.payload.key)],
			};

		case ACTION_BORED_SET_ERROR:
			return {
				...state,
				loading: false,
				error: action.payload,
			};

		case ACTION_BORED_SET_PARTICIPANTS:
			return {
				...state,
				participants: action.payload,
			};

		case ACTION_BORED_SET_ACTIVITY_TYPE:
			return {
				...state,
				activityType: action.payload,
			};

		default:
			return state;
	}
};
