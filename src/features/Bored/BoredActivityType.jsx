const ACTIVITY_TYPES = [
	"education",
	"recreational",
	"social",
	"diy",
	"charity",
	"cooking",
	"relaxation",
	"music",
	"busywork",
];

function BoredActivityType({ onChange }) {
	return (
		<section className="mb-4 border-b border-slate-300 pb-4">
			<label htmlFor="activity-type" className="block mb-4">
				Anything specific?
			</label>
			<select
				id="activity-type"
				onChange={onChange}
				defaultValue="0"
				className="capitalize border-2 border-slate-200 p-4 rounded-lg">
				<option disabled value="0">
					Choose an activity type
				</option>
				{ACTIVITY_TYPES.map((activity) => (
					<option key={activity} value={activity}>
						{activity}
					</option>
				))}
			</select>
		</section>
	);
}
export default BoredActivityType;
