const MESSAGES = [
	"Oh! Something is happening!",
	"Zzz... I really need something fun",
	"Wow! You clicked a button? You must be bored?",
	"Beep Boop Beep, Finding Activity...",
	"Look at you! Making some effort for a change",
	"Congratulations! You took the first step to escape boredom.",
];

const getRandom = (messages) => {
	return messages[Math.floor(Math.random() * messages.length)];
};

function BoredLoading() {
	const message = getRandom(MESSAGES);
	return (
		<section className="border-2 border-slate-400 rounded p-4 mb-4">
			<p>{message}</p>
		</section>
	);
}
export default BoredLoading;
