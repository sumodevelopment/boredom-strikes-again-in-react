import { useReducer } from "react";
import BoredActivity from "./BoredActivity";
import BoredActivityType from "./BoredActivityType";
import { findActivity } from "./BoredAPI";
import BoredButton from "./BoredButton";
import BoredHeader from "./BoredHeader";
import BoredHistory from "./BoredHistory";
import BoredLoading from "./BoredLoading";
import BoredParticipants from "./BoredParticipants";
import {
	boredFetchAction,
	boredReducer,
	boredSetActivityAction,
	boredSetActivityType,
	boredSetErrorAction,
	boredSetParticipants,
	INITIAL_STATE,
} from "./BoredState";

function Bored() {
	console.log("Bored.render()");
	const [state, dispatch] = useReducer(boredReducer, INITIAL_STATE);

	const onBoredClick = async () => {
		dispatch(boredFetchAction());
		const [error, activity] = await findActivity(state);
		if (error !== null) {
			dispatch(boredSetErrorAction(error));
		} else {
			dispatch(boredSetActivityAction(activity));
		}
	};

	const onActivityTypeChange = (event) => {
		const { value } = event.target;
		dispatch(boredSetActivityType(value));
	};

	const onParticipantsChange = (participants) => {
		dispatch(boredSetParticipants(participants));
	};

	return (
		<div className="h-[100vh] grid grid-cols-[2fr_10fr] grid-rows-1 overflow-hidden">
			<aside className="p-8 bg-blue-500 text-white h-full max-w-[350px]">
				<BoredHeader />
			</aside>
			<main className="p-8 overflow-y-auto">
				<section className="mb-4 bg-slate-200 p-4 rounded-lg">
					<BoredActivityType onChange={onActivityTypeChange} />
					<BoredParticipants onParticipantsChange={onParticipantsChange} />
					<BoredButton onClick={onBoredClick}>I'm Bored</BoredButton>
				</section>
				{state.loading && <BoredLoading />}
				<BoredActivity activity={state.activity} />
				<BoredHistory history={state.history} />
			</main>
		</div>
	);
}
export default Bored;
