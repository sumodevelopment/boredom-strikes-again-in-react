import BoredPricing from "./BoredPricing";

function BoredActivity({ activity }) {
	if (activity === null) {
		return <></>;
	}

	return (
		<section className="bg-white rounded-lg border-2 border-slate-300 p-8">
			<header className="mb-6">
				<span className="text-md uppercase text-slate-400">Type: {activity.type}</span>
				<h4 className="text-2xl">{activity.activity}</h4>
			</header>

			<p className="text-slate-500 mb-2">
				You need {activity.participants} participant(s) to do this
			</p>
			<div>
				<span className="block">Pricing</span>
				<BoredPricing price={activity.price} />
			</div>
		</section>
	);
}

export default BoredActivity;
