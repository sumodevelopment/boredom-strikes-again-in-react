function BoredPricing({ price = 0 }) {

	const pricing = [];
	const limit = Math.ceil((price * 10) / 2);

	for (let i = 0; i < limit; i++) {
		pricing.push(<span key={`price-${i}`}>💰</span>);
	}

	return (
		<div>
			{pricing.length === 0 && <span>Free 🥳</span>}
			{pricing}
		</div>
	);
}
export default BoredPricing;
