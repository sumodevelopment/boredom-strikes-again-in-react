import { useState } from "react";

function BoredParticipants({ onParticipantsChange }) {
	const [participants, setParticipants] = useState(0);

	const onIncrease = () => {
		const count = participants + 1;
		onParticipantsChange(count);
		setParticipants(count);
	};

	const onDecrease = () => {
		if (participants === 0) return;
		const count = participants - 1;
		onParticipantsChange(count);
		setParticipants(count);
	};

	return (
		<section className="mb-4 border-b border-slate-300 pb-4">
			<label htmlFor="participants" className="block mb-4">
				How many people?
			</label>
			<button
				onClick={onDecrease}
				className="w-12 h-12 rounded-full bg-blue-500 text-white font-bold border-2 border-slate-900">
				-
			</button>
			<span className="font-semibold text-lg p-4 mx-2 border-2 rounded border-slate-200">
				{participants}
			</span>
			<button
				onClick={onIncrease}
				className="w-12 h-12 rounded-full bg-blue-500 text-white font-bold border-2 border-slate-900">
				+
			</button>
		</section>
	);
}

export default BoredParticipants;
