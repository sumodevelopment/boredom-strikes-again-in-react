import axios from "axios";
const BASE_URL = "https://www.boredapi.com/api/activity";

export async function findActivity({ activityType, participants, price }) {
	let queryParams = [];
	let url = BASE_URL;

	if (activityType) {
		queryParams.push("type=" + activityType);
	}

	if (participants) {
		queryParams.push("participants=" + participants);
	}

	if (queryParams.length > 0) {
		url += "?" + queryParams.join("&");
	}

	try {
		const { data } = await axios.get(url);
		return [null, data];
	} catch (error) {
		return [error.message, null];
	}
}
