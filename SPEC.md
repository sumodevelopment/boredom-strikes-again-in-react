# SPEC

## Boredom Strikes Again

API URL: [https://www.boredapi.com](https://www.boredapi.com)

Use the API tcreate a React app.

### Main Requirements

The app must meet the following requirements:

- There should be a button to get a new activity
- It should display the activity with the returned inform the request (activity, type, participants, price)
- Clicking the button should clear the previous activity and show a new one
- There should be appropriate loading messages

### Optional

Check the documentation for more information on adding filter the request:
<https://www.boredapi.com/documentation>

Have inputs to choose

- Number of participants
- Select type of activity
- Available Types:
  - education
  - recreational
  - social
  - diy
  - charity
  - cooking
  - relaxation
  - music
  - busywork”
- Set min and max price
